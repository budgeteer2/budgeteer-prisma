CREATE TABLE "public"."User" (
    id SERIAL PRIMARY KEY NOT NULL,
    email VARCHAR(255),
    "firstName" VARCHAR(255),
    password VARCHAR(255)
);

CREATE TABLE "public"."Budget" (
    id SERIAL PRIMARY KEY NOT NULL,
    name VARCHAR(255),
    icon VARCHAR(255),
    "userId" INTEGER NOT NULL,
    FOREIGN KEY ("userId") REFERENCES "public"."User"(id)
);

CREATE TABLE "public"."BudgetItem" (
    id SERIAL PRIMARY KEY NOT NULL,
    name VARCHAR(255),
    "budgetId" INTEGER NOT NULL,
    FOREIGN KEY ("budgetId") REFERENCES "public"."Budget"(id)
);

CREATE TABLE "public"."BudgetItemData" (
    id SERIAL PRIMARY KEY NOT NULL,
    "amount" DECIMAL NOT NULL DEFAULT 0.00,
    "spent" DECIMAL NOT NULL DEFAULT 0.00,
    "isLocked" BOOLEAN NOT NULL DEFAULT false,
    "date" date NOT NULL,
    "budgetItemId" INTEGER NOT NULL,
    FOREIGN KEY ("budgetItemId") REFERENCES "public"."BudgetItem"(id)
)