import "reflect-metadata";
import express from "express";
import { graphqlHTTP } from "express-graphql";
import { resolvers } from "./prisma/type-graphql";
import cors from 'cors';
import { createContext } from "./context";
import { buildSchema } from "type-graphql";

async function main() {
	const app = express();

	const schema = await buildSchema({ resolvers, validate: false });

	const context = createContext();

	app.use(cors());

	app.use(
		"/graphql",
		graphqlHTTP({
			schema,
            context,
            graphiql: true
		})
    );
    
    app.listen(8081, () => {
        console.log('App listening on localhost:8081')
    });
}

main()